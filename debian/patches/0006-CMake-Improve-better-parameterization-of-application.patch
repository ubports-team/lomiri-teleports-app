From 45e3bb73fa0f7a8c90b10b305478fa4e01f70ef4 Mon Sep 17 00:00:00 2001
From: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
Date: Fri, 27 Dec 2024 11:17:50 +0100
Subject: [PATCH 06/13] CMake: Improve better parameterization of application
 name.

Signed-off-by: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
---
 CMakeLists.txt                             | 10 ++++++++
 app/CMakeLists.txt                         |  8 +++---
 app/main.cpp                               | 21 ++++++++-------
 app/qml/Main.qml                           |  2 +-
 app/qml/stores/NotificationsStateStore.qml |  2 +-
 common/CMakeLists.txt                      |  3 ++-
 common/utils/CMakeLists.txt                |  8 ++++++
 common/utils/utils.cpp                     | 30 ++++++++++++++++++++++
 common/utils/utils.h                       |  7 +++++
 config.h.in                                | 16 ++++++++++++
 libs/qtdlib/CMakeLists.txt                 | 11 +++++---
 libs/qtdlib/client/qtdclient.cpp           |  6 +++--
 manifest.json.in                           |  4 +--
 push/CMakeLists.txt                        |  2 +-
 push/config.h                              | 15 -----------
 push/push.cpp                              | 14 +++++++---
 push/pushhelper.cpp                        |  3 ++-
 17 files changed, 117 insertions(+), 45 deletions(-)
 create mode 100644 common/utils/CMakeLists.txt
 create mode 100644 common/utils/utils.cpp
 create mode 100644 common/utils/utils.h
 create mode 100644 config.h.in
 delete mode 100644 push/config.h

--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -39,11 +39,13 @@
 set(QT_IMPORTS_DIR "lib/${ARCH_TRIPLET}")
 
 set(PROJECT_NAME "teleports")
+set(APP_PRETTYNAME "TELEports")
 set(FULL_PROJECT_NAME "teleports.ubports")
 set(ICON_FILE         "icon.svg")
 set(SPLASH_FILE       "splash.svg")
 
 if(CLICK_MODE)
+    set(CLICK_MODE_ENABLED   1)
     set(APP_HARDCODE         "${PROJECT_NAME}")
     set(DATA_DIR             "${CMAKE_INSTALL_PREFIX}")
     set(ICON                 "${DATA_DIR}/assets/${ICON_FILE}")
@@ -55,6 +57,7 @@
     set(PUSH_DIR             "${DATA_DIR}/push")
     set(PUSH_EXEC            "push")
 else(CLICK_MODE)
+    set(CLICK_MODE_ENABLED   0)
     set(APP_HARDCODE         "lomiri-teleports-app")
     set(DATA_DIR             "${CMAKE_INSTALL_FULL_DATADIR}/${APP_HARDCODE}")
     set(ICON                 "${DATA_DIR}/assets/${ICON_FILE}")
@@ -71,6 +74,13 @@
 set(DESKTOP_FILE_NAME ${PROJECT_NAME}.desktop)
 set(CURRENT_LIBRARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/libs)
 
+# config file
+
+configure_file("${CMAKE_CURRENT_SOURCE_DIR}/config.h.in"
+               "${CMAKE_CURRENT_BINARY_DIR}/config.h"
+               IMMEDIATE @ONLY)
+
+
 # Sets BUILD_VERSION: Either tag of the current git HEAD or devel build version with git hash
 execute_process(
   COMMAND git describe --tags --abbrev=0 --exact-match
--- a/app/CMakeLists.txt
+++ b/app/CMakeLists.txt
@@ -8,13 +8,11 @@
 
 #"qtdclient.cpp" "qtdthread.cpp" "qml.qrc"
 add_definitions(-DBUILD_VERSION="${BUILD_VERSION}" -DGIT_HASH="${GIT_HASH}" -DQT_MESSAGELOGCONTEXT)
+add_executable(${APP_HARDCODE} "main.cpp" "messagedelegatemap.cpp" "messagecontentdelegatemap.cpp" "ui-tools/offloadingimageprovider.cpp" "qml/qml.qrc" "qml/icons/icons.qrc")
 if(CLICK_MODE)
-    add_executable(${APP_HARDCODE} "main.cpp" "messagedelegatemap.cpp" "messagecontentdelegatemap.cpp" "ui-tools/offloadingimageprovider.cpp" "qml/qml.qrc" "qml/icons/icons.qrc")
-    add_definitions(-DDO_QUICKFLUX_REGISTERTYPE)
-    target_link_libraries(${APP_HARDCODE} Qt5::Core Qt5::Quick Qt5::QuickControls2 QuickFlux::quickflux QTdlib rLottieQml::rLottieQml)
+    target_link_libraries(${APP_HARDCODE} Qt5::Core Qt5::Quick Qt5::QuickControls2 QuickFlux::quickflux QTdlib rLottieQml::rLottieQml utils)
 else(CLICK_MODE)
-    add_executable(${APP_HARDCODE} "main.cpp" "messagedelegatemap.cpp" "messagecontentdelegatemap.cpp" "ui-tools/offloadingimageprovider.cpp" "qml/qml.qrc" "qml/icons/icons.qrc")
-    target_link_libraries(${APP_HARDCODE} Qt5::Core Qt5::Quick Qt5::QuickControls2 QTdlib rLottieQml::rLottieQml)
+    target_link_libraries(${APP_HARDCODE} Qt5::Core Qt5::Quick Qt5::QuickControls2 QTdlib rLottieQml::rLottieQml utils)
 endif(CLICK_MODE)
 
 install(TARGETS ${APP_HARDCODE} RUNTIME DESTINATION ${CMAKE_INSTALL_FULL_BINDIR})
--- a/app/main.cpp
+++ b/app/main.cpp
@@ -3,9 +3,10 @@
 #include <QJsonObject>
 #include <QDebug>
 #include <QQmlContext>
-#ifdef DO_QUICKFLUX_REGISTERTYPE
+#if TELEPORTS_APP_CLICK_MODE
 #include <QuickFlux>
-#endif /* DO_QUICKFLUX_REGISTERTYPE */
+#define QUICK_FLUX_DISABLE_AUTO_QML_REGISTER
+#endif /* TELEPORTS_APP_CLICK_MODE */
 #include <quick/plugin.h>
 #include <QtQuickControls2/QQuickStyle>
 #include "messagedelegatemap.h"
@@ -16,7 +17,8 @@
 #include <ui-tools/offloadingimageprovider.h>
 #include <QQuickView>
 
-#define QUICK_FLUX_DISABLE_AUTO_QML_REGISTER
+#include "../common/utils/utils.h"
+#include "../config.h"
 
 int main(int argc, char *argv[])
 {
@@ -25,14 +27,14 @@
     qSetMessagePattern("%{time} %{type} %{if-category}%{category}: %{endif}(%{function}) %{message}");
     QQuickStyle::setStyle("Suru");
 
-    QCoreApplication::setApplicationName(QStringLiteral("teleports.ubports"));
-    QCoreApplication::setOrganizationName(QStringLiteral("teleports.ubports"));
-    QCoreApplication::setOrganizationDomain(QStringLiteral("teleports.ubports"));
-    QGuiApplication::setApplicationDisplayName(QStringLiteral("TELEports"));
+    QCoreApplication::setApplicationName(QStringLiteral(SETTINGS_APP_NAME));
+    QCoreApplication::setOrganizationName(QStringLiteral(SETTINGS_ORGANIZATION_NAME));
+    QCoreApplication::setOrganizationDomain(QStringLiteral(SETTINGS_ORGANIZATION_DOMAIN));
+    QGuiApplication::setApplicationDisplayName(QStringLiteral(SETTINGS_APP_PRETTYNAME));
 
-#ifdef DO_QUICKFLUX_REGISTERTYPE
+#if TELEPORTS_APP_CLICK_MODE
     registerQuickFluxQmlTypes();
-#endif /* DO_QUICKFLUX_REGISTERTYPE */
+#endif /* TELEPORTS_APP_CLICK_MODE */
     QTdLib::registerQmlTypes();
 
     setlocale(LC_ALL, "");
@@ -45,6 +47,7 @@
 
     QQuickView *view = new QQuickView();
 
+    view->rootContext()->setContextProperty("appName", SETTINGS_APP_NAME);
     view->rootContext()->setContextProperty("applicationDirPath", QGuiApplication::applicationDirPath());
     view->rootContext()->setContextProperty(QStringLiteral("delegateMap"), &delegateMap);
     view->rootContext()->setContextProperty(QStringLiteral("contentDelegateMap"), &contentDelegateMap);
--- a/app/qml/Main.qml
+++ b/app/qml/Main.qml
@@ -26,7 +26,7 @@
     readonly property bool tablet: landscape ? width >= units.gu(90) : height >= units.gu(90)
     theme.name: Telegram.settingsStore.uitkTheme
     anchorToKeyboard: true
-    applicationName: "teleports.ubports"
+    applicationName: root.appName
 
     Item {
         id: rootItem
--- a/app/qml/stores/NotificationsStateStore.qml
+++ b/app/qml/stores/NotificationsStateStore.qml
@@ -15,7 +15,7 @@
 
     PushClient {
         id: pushClient
-        appId: "teleports.ubports_teleports"
+        appId: root.appName + "_teleports"
         onTokenChanged: console.log("Got push token: ", token)
         onError: {
             if ( error === "bad auth" && !settingsStore.u1DialogShown ) {
--- a/common/CMakeLists.txt
+++ b/common/CMakeLists.txt
@@ -1 +1,2 @@
-add_subdirectory(auxdb)
\ No newline at end of file
+add_subdirectory(auxdb)
+add_subdirectory(utils)
--- /dev/null
+++ b/common/utils/CMakeLists.txt
@@ -0,0 +1,8 @@
+find_package(Qt5 COMPONENTS Core REQUIRED)
+
+set(SOURCES
+    utils.cpp
+)
+
+add_library(utils STATIC ${SOURCES} )
+target_link_libraries(utils Qt5::Core)
--- /dev/null
+++ b/common/utils/utils.cpp
@@ -0,0 +1,30 @@
+#include <QDebug>
+
+#include "utils.h"
+
+#include "../../config.h"
+
+bool utilsClickModeEnabled()
+{
+    return (QString::number(TELEPORTS_APP_CLICK_MODE) == "1");
+}
+
+QString utilsFullDataPath(const QString &appPath, const QString &fileName)
+{
+    QString result;
+    if (qEnvironmentVariableIsSet(SNAP_PATH)) {
+        result = qgetenv(SNAP_PATH) + QStringLiteral("/usr/share/") + SETTINGS_APP_NAME + QStringLiteral("/") + fileName;
+    } else if (appPath.startsWith(TELEPORTS_APP_DEV_BINDIR)) {
+        result = QString(TELEPORTS_APP_DEV_DATADIR) + QStringLiteral("/") + fileName;
+    } else if (utilsClickModeEnabled()) {
+        if (appPath.endsWith("/push")) {
+            result = appPath + QStringLiteral("/../share/") + SETTINGS_APP_NAME + QStringLiteral("/") + fileName;
+        }
+        else {
+            result = appPath + QStringLiteral("/share/") + SETTINGS_APP_NAME + QStringLiteral("/") + fileName;
+        }
+    } else {
+        result = QString(TELEPORTS_APP_INSTALL_DATADIR) + QStringLiteral("/") + fileName;
+    }
+    return result;
+}
--- /dev/null
+++ b/common/utils/utils.h
@@ -0,0 +1,7 @@
+#ifndef UTILS_H
+#define UTILS_H
+
+bool utilsClickModeEnabled();
+QString utilsFullDataPath(const QString &appPath, const QString &fileName);
+
+#endif // UTILS_H
\ No newline at end of file
--- /dev/null
+++ b/config.h.in
@@ -0,0 +1,16 @@
+#ifndef __CONFIG_H__
+#define __CONFIG_H__
+
+#define TELEPORTS_APP_CLICK_MODE            @CLICK_MODE_ENABLED@
+#define TELEPORTS_APP_INSTALL_DATADIR       "@DATA_DIR@"
+#define TELEPORTS_APP_DEV_DATADIR           "@teleports_SOURCE_DIR@"
+#define TELEPORTS_APP_DEV_BINDIR            "@CMAKE_BINARY_DIR@"
+
+#define SETTINGS_ORGANIZATION_NAME          "@FULL_PROJECT_NAME@"
+#define SETTINGS_ORGANIZATION_DOMAIN        "UBports"
+#define SETTINGS_APP_NAME                   "@APP_HARDCODE@"
+#define SETTINGS_APP_PRETTYNAME             "@APP_PRETTYNAME@"
+
+#define SNAP_PATH                           "PATH"
+
+#endif
--- a/libs/qtdlib/CMakeLists.txt
+++ b/libs/qtdlib/CMakeLists.txt
@@ -1,8 +1,13 @@
 project(qtdlib C CXX)
 cmake_minimum_required(VERSION 3.0.0)
 
-set(DEVICE_MODEL "Ubuntu Touch")
-set(SYSTEM_VERSION "unknown")
+if(CLICK_MODE)
+    set(DEVICE_MODEL "Ubuntu Touch")
+    set(SYSTEM_VERSION "unknown")
+else(CLICK_MODE)
+    set(DEVICE_MODEL "TELEports")
+    set(SYSTEM_VERSION ${PROJECT_VERSION})
+endif(CLICK_MODE)
 
 if (DEFINED ENV{SDK_FRAMEWORK})
     string(REGEX MATCH "[0-9]+\\.[0-9]+" SYSTEM_VERSION  $ENV{SDK_FRAMEWORK})
@@ -280,7 +285,7 @@
 else(CLICK_MODE)
     add_library(${QTDLIB} STATIC ${SRC})
 endif(CLICK_MODE)
-target_link_libraries(${QTDLIB} Qt5::Core Qt5::Quick Qt5::Widgets Qt5::Qml Qt5::Concurrent Qt5::Positioning Qt5::Multimedia Td::tdjson auxdb)
+target_link_libraries(${QTDLIB} Qt5::Core Qt5::Quick Qt5::Widgets Qt5::Qml Qt5::Concurrent Qt5::Positioning Qt5::Multimedia Td::tdjson auxdb utils)
 target_include_directories(${QTDLIB} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
 include_directories(${THUMBNAILER_INCLUDE_DIRS})
 target_link_libraries(${QTDLIB} ${THUMBNAILER_LIBRARIES})
--- a/libs/qtdlib/client/qtdclient.cpp
+++ b/libs/qtdlib/client/qtdclient.cpp
@@ -15,6 +15,8 @@
 #include "connections/qtdconnectionstatefactory.h"
 #include "../../common/auxdb/auxdb.h"
 #include "../../common/auxdb/avatarmaptable.h"
+#include "../../common/utils/utils.h"
+#include "../../config.h"
 
 QJsonObject execTd(const QJsonObject &json, const bool debug)
 {
@@ -45,8 +47,8 @@
     , m_connectionState(Q_NULLPTR)
     , m_tagcounter(0)
     , m_auxdb(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation).append("/auxdb"),
-              QGuiApplication::applicationDirPath().append("/assets"), this)
-    , m_postalClient("teleports.ubports_teleports")
+              utilsFullDataPath(QGuiApplication::applicationDirPath(), "/assets"), this)
+    , m_postalClient(SETTINGS_APP_NAME)
     , m_debug(false)
 {
     if (!m_debug) {
--- a/manifest.json.in
+++ b/manifest.json.in
@@ -1,8 +1,8 @@
 {
-    "name": "teleports.ubports",
+    "name": "@FULL_PROJECT_NAME@",
     "description": "An Ubuntu Touch Telegram client",
     "architecture": "$ENV{ARCH}",
-    "title": "TELEports",
+    "title": "@APP_PRETTYNAME@",
     "hooks": {
         "teleports": {
             "apparmor": "teleports.apparmor",
--- a/push/CMakeLists.txt
+++ b/push/CMakeLists.txt
@@ -9,7 +9,7 @@
 
 add_executable(push ${SOURCES} )
 
-target_link_libraries(push Qt5::Core Qt5::Widgets Qt5::Network Qt5::DBus Qt5::Sql auxdb)
+target_link_libraries(push Qt5::Core Qt5::Widgets Qt5::Network Qt5::DBus Qt5::Sql auxdb utils)
 
 if(CLICK_MODE)
     install(FILES push-apparmor.json DESTINATION ${PUSH_DIR})
--- a/push/config.h
+++ /dev/null
@@ -1,15 +0,0 @@
-#pragma once
-
-// TODO This is shared with scope and should be put to a shared class.
-
-#include "i18n.h"
-
-const bool DEBUG = false;
-
-const QString CONFIG_PATH = "/home/phablet/.config/teleports.ubports";
-const QString CACHE_PATH = "/home/phablet/.cache/teleports.ubports";
-const QString PROFILES_PATH = CONFIG_PATH + "/profiles.sqlite";
-const QString DATABASE_PATH_FMT = CONFIG_PATH + "/%1/database.db";
-
-const QString PROFILE_DIR_FMT = CACHE_PATH + "/%1/downloads/%2/profile";
-const QString PROFILE_FILE_FMT = "file://" + CACHE_PATH + "/%1/downloads/%2/profile/%3";
--- a/push/push.cpp
+++ b/push/push.cpp
@@ -5,6 +5,8 @@
 
 #include "pushhelper.h"
 
+#include "../config.h"
+
 int main(int argc, char *argv[])
 {
     if (argc != 3) {
@@ -14,12 +16,16 @@
     QCoreApplication app(argc, argv);
     QStringList args = app.arguments();
 
-    QCoreApplication::setApplicationName(QStringLiteral("teleports.ubports"));
-    QCoreApplication::setOrganizationName(QStringLiteral("teleports.ubports"));
-    QCoreApplication::setOrganizationDomain(QStringLiteral("teleports.ubports"));
+#if TELEPORTS_APP_CLICK_MODE
+    QCoreApplication::setApplicationName(QStringLiteral("push"));
+#else  /* TELEPORTS_APP_CLICK_MODE */
+    QCoreApplication::setApplicationName(QStringLiteral(SETTINGS_APP_NAME));
+#endif /* TELEPORTS_APP_CLICK_MODE */
+    QCoreApplication::setOrganizationName(QStringLiteral(SETTINGS_ORGANIZATION_NAME));
+    QCoreApplication::setOrganizationDomain(QStringLiteral(SETTINGS_ORGANIZATION_DOMAIN));
 
     QLoggingCategory::setFilterRules("auxdb=false");
-    PushHelper pushHelper("teleports.ubports_teleports", // no-i18n
+    PushHelper pushHelper(SETTINGS_APP_NAME + QStringLiteral("_teleports"), // no-i18n
                           QString(args.at(1)), QString(args.at(2)), &app);
 
     QObject::connect(&pushHelper, SIGNAL(done()), &app, SLOT(quit()));
--- a/push/pushhelper.cpp
+++ b/push/pushhelper.cpp
@@ -1,5 +1,6 @@
 #include "pushhelper.h"
 #include "../common/auxdb/avatarmaptable.h"
+#include "../common/utils/utils.h"
 #include "i18n.h"
 #include <QApplication>
 #include <QDebug>
@@ -17,7 +18,7 @@
     , mOutfile(outfile)
     , m_postalClient(new PostalClient(appId))
     , m_auxdb(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation).append("/auxdb"),
-              QGuiApplication::applicationDirPath().append("/assets"), this)
+              utilsFullDataPath(QGuiApplication::applicationDirPath(), "assets"), this)
 {
     setlocale(LC_ALL, "");
     textdomain(GETTEXT_DOMAIN.toStdString().c_str());
